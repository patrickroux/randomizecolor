### What is this repository for? ###

* Une astuce pour changer simplement et facilement de couleur au survol
* Version 1.0

### How do I get set up? ###

Dans les fichiers de l'exemple, ne retenir que randomizeColor.js.
Le reste sert à l'exemple. Vous aurez bien sur besoin de JQuery aussi.


```
#!javascript
$(document).ready(function() {
    //All attributes are optional
    $('myelement').randomizeColor({
        speed : 500, 
        // OR "fast", "slow", ...
        property : "backgroundColor",
        // OR "color" or "all" 
        infinite : true,
        //If don't want to stop changing while mouse is over
        definedColors : ["#FF8000", "#AA0070"],
        //Do you want to choose random just between these?
    });  
 });
```

 


### Contribution guidelines ###

* Writing tests
* Code review
* Other guidelines